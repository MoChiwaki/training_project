angular.module('myapp', [])
  .controller('displayCntrl', function($scope){
    $scope.firstText = '';
  })

  .controller('listCntrl', function($scope){
    // $scope.secondText='';
    $scope.listText=[];
    $scope.addListText = function(){
      if(!$scope.secondText) return;
      if(!$scope.numberText){
        $scope.numberText = 0;
      }
      console.log($scope.secondText);
      $scope.listText.push({
        secondText: $scope.secondText,
        numberText: $scope.numberText
      });
      $scope.secondText = '';
      $scope.numberText = '';
    }
    $scope.deleteText = function(index){
      $scope.listText.splice(index, 1);
    }
  });
